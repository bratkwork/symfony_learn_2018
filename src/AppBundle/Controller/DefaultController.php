<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('@App/default/index.html.twig');
    }


    /**
     * @Route("/feedback", name="feedback")
     */
    public function feedbackAction()
    {
        return $this->render('@App/default/feedback.html.twig');
    }


    /**
     * @Route("/homepage_lesson_2", name="homepage_lesson_2")
     * @param Request $request
     * @return Response
     */
    public function indexLessonTwoAction(Request $request)
    {
        $a = 123;
        $someArray = [1, 2, 3];
        $someValue = false;

        return $this->render('@App/default/index_before_4_lesson.html.twig', [
            'a' => $a,
            'some_array' => $someArray,
            'some_value' => $someValue
        ]);
    }


    public function layoutBootstrapCdnAction(Request $request) {

        return $this->render('@App/layout_bootstrap_cdn.html.twig');
    }

}
