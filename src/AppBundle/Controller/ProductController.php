<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="product_list")
     * @Template()
    */
    public function indexAction()
    {
        $products = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->findActive()
        ;

        return ['products' => $products];
    }


    /**
     * @Route("/product/{id}", name="product_item", requirements={"id": "[0-9]+"})
     * @Template()
     * 
     * @param $id
     * @return array
     */
    public function showAction(Product $id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);
        
        return ['product' => $product];
    }


    /**
     * @Route("/product_no_entity_mapping/{id}", name="product_item_product_no_entity_mapping", requirements={"id": "[0-9]+"})
     * @Template("@App/product/show.html.twig")
     * 
     * @param $id
     * @return array
     */
    public function productShowNoEntityMappingAction($id)
    {
        $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($id);

        if (!$product) {
            throw $this->createNotFoundException('Product not found');
        }

        return ['product' => $product];
    }
}
