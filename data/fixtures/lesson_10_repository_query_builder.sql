--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: category; Type: TABLE; Schema: public; Owner: weezuu
--

CREATE TABLE public.category (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.category OWNER TO weezuu;

--
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: weezuu
--

CREATE SEQUENCE public.category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO weezuu;

--
-- Name: product; Type: TABLE; Schema: public; Owner: weezuu
--

CREATE TABLE public.product (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    price numeric(10,2) NOT NULL,
    description text,
    active boolean DEFAULT true NOT NULL,
    category_id integer
);


ALTER TABLE public.product OWNER TO weezuu;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: weezuu
--

CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO weezuu;

--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: weezuu
--

COPY public.category (id, name) FROM stdin;
1	framework
2	support
3	open architecture
4	Synchronized
5	Stable
6	Multi-channelled
7	matrix
8	static
9	Devolved
10	toolset
11	books
\.


--
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: weezuu
--

SELECT pg_catalog.setval('public.category_id_seq', 1, false);


--
-- Data for Name: product; Type: TABLE DATA; Schema: public; Owner: weezuu
--

COPY public.product (id, title, price, description, active, category_id) FROM stdin;
1	Carrie	123.00	null	t	11
2	Idiot	200.00	Very famous	f	11
\.


--
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: weezuu
--

SELECT pg_catalog.setval('public.product_id_seq', 1, false);


--
-- Name: category_pkey; Type: CONSTRAINT; Schema: public; Owner: weezuu
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- Name: product_pkey; Type: CONSTRAINT; Schema: public; Owner: weezuu
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: idx_d34a04ad12469de2; Type: INDEX; Schema: public; Owner: weezuu
--

CREATE INDEX idx_d34a04ad12469de2 ON public.product USING btree (category_id);


--
-- Name: uniq_64c19c15e237e06; Type: INDEX; Schema: public; Owner: weezuu
--

CREATE UNIQUE INDEX uniq_64c19c15e237e06 ON public.category USING btree (name);


--
-- Name: fk_d34a04ad12469de2; Type: FK CONSTRAINT; Schema: public; Owner: weezuu
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fk_d34a04ad12469de2 FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

